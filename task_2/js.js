let arrWords = ['red', 'blue', 'green', 'yellow', 'lime', 'magenta', 'black', 'gold', 'gray', 'tomato'];
let arrColors = [];
let values = [];
let arr = [];
let counter = 0;

while (counter !== 25) createNumber();

function createNumber() {
    let rand = 0 + Math.random() * arrWords.length;
    let color = 0 + Math.random() * arrWords.length;
    let flag = false;
    checkConditions(Math.floor(rand),flag,Math.floor(color));
}

function checkConditions(rand,flag,color) {
    for (let i = 0; i < values.length; i++) {
        if (rand === values[i] || rand === color) flag = true;
    }

    if (flag === true) createNumber();
    else {
        values.push(rand);
        arrColors.push(color);
        counter++;
    }

    if (counter % 5 === 0 && counter >= 5) {

        for (let i = 0; i < values.length; i++) {
            arr.push(values[i]);
        }

        values = [];
    }
}

(function createStrings() {
    let wrapper = document.querySelector('.wrapper');

    for (let i = 0; i < arr.length; i++) {
        counter = i + 1;
        let color;
        let span = document.createElement('span');
        span.innerHTML = arrWords[arr[i]];
        color = Math.floor(0 + Math.random() * arrWords.length);
        span.style.color = String(arrWords[arrColors[i]]);
        span.style.padding = '5px';
        wrapper.appendChild(span);

        if (counter % 5 === 0 && counter >= 5) {
            let br = document.createElement('br');
            wrapper.appendChild(br);
        }
    }
})();