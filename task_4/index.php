<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body style="display: flex; flex-direction: column">
    <form action="index.php" method="post">
        <form method="POST">
            <input type='text' name='team-search'>
            <input type="submit" name="btn" value="OK" />
        </form>
    </form>
</body>
</html>
<style>
</style>
<?php
include 'simple_html_dom.php';
$url = ['https://terrikon.com/football/italy/championship/table',
    'https://terrikon.com/football/italy/championship/2018-19/table',
    'https://terrikon.com/football/italy/championship/2017-18/table',
    'https://terrikon.com/football/italy/championship/2016-17/table',
    'https://terrikon.com/football/italy/championship/2015-16/table',
    'https://terrikon.com/football/italy/championship/2013-14/table',
    'https://terrikon.com/football/italy/championship/2012-13/table',
    'https://terrikon.com/football/italy/championship/2011-12/table',
    'https://terrikon.com/football/italy/championship/2010-11/table',
    'https://terrikon.com/football/italy/championship/2009-10/table'];

$year = [];
$place = [];

if(isset($_POST['btn'])) {
    $a = $_POST['team-search'];

    for ($i = 0; $i < count($url); $i++) {
        $html = file_get_html("$url[$i]");

        foreach($html->find('.id') as $element) {
            array_push($year,$element);
        }

        foreach($html->find('.colored') as $element) {

            foreach ($element->find("tr") as $item) {
                if ($a == $item->children(1)->children(0)->plaintext) {
                    array_push($place,$item->find("text", 1) . $item->find("a", 0));
                }
            }
        }
    }

    for($i = 0; $i < count($place); $i++) {
        echo $year[$i];
        echo $place[$i];
    }
}