let methods = {
    getClassNameAndLetter() {
        console.log(this.name);
        console.log(this.letter);
    },
};
class First {
    constructor(name,letter) {
        this.name = "First";
        this.letter = "A";
    }
}
class Second {
    constructor(name,letter) {
        this.name = "Second";
        this.letter = "B";
    }
}

Object.assign(First.prototype, methods);
Object.assign(Second.prototype, methods);

new First().getClassNameAndLetter();
new Second().getClassNameAndLetter();